This project is an example of frontend sandbox created with React and Restful API of NASA.

To run this project successfully, you need to use your api key the variable named apiKey in componentDidMount() method of /components/RegionsWithMostSolarFlares.jsx file.
Otherwise, the project will not run at all!

The NASA APIs require you to sign up here (its free), this will allow you approximately 1000 API
calls per hour. You can generate an API key at https://api.nasa.gov/ and select Generate API key.

Start date and end date to fetch data from NASA api are hard coded at this moment.

This project is licensed under Apache License 2.0 and permitted for Commercial use, Modification, Distribution, and Private use
